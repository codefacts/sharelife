import {NativeModules} from 'react-native';
import composer from './core/Composer.js';

module.exports = composer.addAndReturnFactory('JavaModule', [], () => {
  return NativeModules.JavaModule;
});
