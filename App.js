import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

require('./init.js');

import composer from './core/Composer.js';

const AppRoot = composer.get('AppRouter');

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <AppRoot/>
    );
  }
}
