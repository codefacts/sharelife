import composer from './core/Composer.js';

export default composer.addAndReturnFactory('Config', [], function () {

  var config = {
    apiBaseUrl: 'http://192.168.100.6:1582',
    siteTitle: 'Web Panel'
  };

  console.log('config', config);

  return config;
});
