import composer from '../core/Composer.js';
import { AppRegistry, View, Text, TextInput, Button, Alert } from 'react-native';

export default composer.addAndReturnFactory('AuthService', ['LocalStorage', 'Apis', 'HttpClient'], function AuthService(args) {

	const {LocalStorage, Apis, HttpClient} = args || {};

	var authData;

	const loadAuthData = () => {
		return  LocalStorage.get('authData')
			.then(ad => authData = ad);
	};

	const isAuthenticated = function () {
		if (!!authData) {
			return Promise.resolve(true);
		}
		return loadAuthData().then(ad => !!ad);
	};

	const login = function ({user, password}) {
		return new Promise(function (resolve, reject) {

			HttpClient.post(
				Apis.loginApi,
				{
					user, password
				}
			)
			.then(res => res.data)
			.then(userObj => {
				return LocalStorage.set('authData', userObj).then(() => authData = userObj);
			})
			.then(userObj => {
				resolve(userObj);
				return userObj;
			})
			.catch(err => {
				if (!!err.response && !!err.response.data) {
					reject(new Error(err.response.data.message));
					return;
				}
				reject(err);
			})
			;

		});
	};

	const logout = function () {
		return HttpClient.get(Apis.logoutApi).then(() => LocalStorage.clear('authData'));
	};

	const getUser = function () {

		if (!!authData) {
			return Promise.resolve(authData);
		}

		return authData;
	};

	const authToken = () => {
		return authData.authToken;
	};

	return {isAuthenticated, login, logout, getUser, authToken};
});
