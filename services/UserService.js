import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('UserService',
['ServiceBuilder', 'Apis', 'HttpClient'],
({ServiceBuilder, Apis, HttpClient}) => {

  const {usersApi, checkPhoneExistsApi} = Apis;

  const service = ServiceBuilder({
    resourceUri: usersApi,
    fullSelections: [
      'r.id',
      'r.firstName',
      'r.lastName',
      'r.phone',
      'r.gender',
      'r.specialization',
      'r.chamber',
      'r.pictureUri',
      'r.userType'
    ],
    summarySelections: [
      'r.id',
      'r.firstName',
      'r.lastName',
      'r.phone',
      'r.specialization',
      'r.chamber',
      'r.pictureUri',
      'r.userType'
    ]
  });

  const findUserByPhone = (phone) => {
    return service.findAll({
      criteria: {
        op: 'eq',
        arg1: {
          op: 'field',
          arg: 'r.phone'
        },
        arg2: phone
      },
      selections: [
        'r.id',
        'r.firstName',
        'r.lastName',
        'r.phone',
        'r.pictureUri'
      ]
    }).then(page => {
      if (page.data.length === 0) {
        return Promise.reject(new Error('No user found with phone no: ' + phone));
      }
      return page.data[0];
    });
  };

  const checkPhoneExists = (phone) => {
    if (!phone) {
      return Promise.reject(new Error('Phone is required'));
    }
    return HttpClient
      .request({
        url: checkPhoneExistsApi,
        method: 'post',
        data: {phone}
      })
      .then(rsp => {
        return rsp.data;
      });
  };

  return u.merge(service, {findUserByPhone, checkPhoneExists});
});
