import composer from '../core/Composer.js';
import u from '../utils/utils.js';
import { AppRegistry, View, Text, TextInput, Button, Alert } from 'react-native';

composer.addAndReturnFactory('CatchError',
[],
() => {
  return (ex) => {
    console.error('CatchError: ', ex);
    const defMsg = 'Unknown error. Please try again later.';
    if (!ex) {
      Alert.alert('Error', defMsg);
      return;
    }
    if (typeof ex === 'string') {
      Alert.alert('Error', ex);
      return;
    }
    Alert.alert('Error', ex.message || defMsg);
  };
});
