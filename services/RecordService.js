import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('RecordService',
['ServiceBuilder', 'Apis', 'HttpClient'],
({ServiceBuilder, Apis, HttpClient}) => {

  const {recordsApi} = Apis;

  const service = ServiceBuilder({
    resourceUri: recordsApi,
    fullJoinParams: [
      {path: 'r.files', alias: 'f'}
    ],
    fullSelections: [
      'r.id',
      'r.filesCount',
      'r.recordDate',

      'r.patient.id',
      'r.patient.firstName',
      'r.patient.lastName',
      'r.patient.phone',
      'r.patient.userType',
      'r.patient.pictureUri',

      'r.doctor.id',
      'r.doctor.firstName',
      'r.doctor.lastName',
      'r.doctor.phone',
      'r.doctor.specialization',
      'r.doctor.chamber',
      'r.doctor.userType',
      'r.doctor.pictureUri',

      'f.id',
      'f.name',
      'f.description',
      'f.pictureUri',
    ],
    summarySelections: [
      'r.id',
      'r.filesCount',
      'r.recordDate',

      'r.patient.id',
      'r.patient.firstName',
      'r.patient.lastName',
      'r.patient.phone',
      'r.patient.userType',
      'r.patient.pictureUri',

      'r.doctor.id',
      'r.doctor.firstName',
      'r.doctor.lastName',
      'r.doctor.phone',
      'r.doctor.specialization',
      'r.doctor.chamber',
      'r.doctor.userType',
      'r.doctor.pictureUri',
    ]
  });

  const findAll = (query) => {
    return service
      .findAll(query)
      .then(pg => u.merge(pg, {
        data: pg.data.map((item) => u.merge(item, {recordDate: new Date(item.recordDate)}))
      }));
  };

  const findById = (id, cfg) => service.findById(id, cfg).then(record => u.merge(record, {recordDate: new Date(record.recordDate)}))

  return u.merge(service, {findAll, findById});
});
