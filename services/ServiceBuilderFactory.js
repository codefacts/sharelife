import composer from '../core/Composer.js';
import u from '../utils/utils.js';

export default composer.addAndReturnFactory('ServiceBuilder',
['HttpClient'],
({HttpClient}) => {

  return (config) => {

    const {headers, resourceUri, alias, fullSelections, fullJoinParams, summarySelections, summaryJoinParams} = u.mergeWithNull({
      headers: {
        'Content-Type': 'application/json; utf-8'
      },
      alias: 'r',
    }, config);

    u.requireNonNull(resourceUri);

    const findById = (id, cfg) => {

      const {selections, joinParams} = cfg || {};

      return HttpClient.request({
        url: resourceUri + '/' + id,
        method: 'get',
        params: u.filterOutNulls({
          selections: (fullSelections && JSON.stringify(selections || fullSelections)),
          joinParams: (fullJoinParams && JSON.stringify(joinParams || fullJoinParams))
        }),
        transformResponse: [parseJson, transformResponse]
      }).then(rsp => rsp.data);
    };

    const findAll = (query) => {
      query = query || {};
      const {page, pageSize, orderBy} = query;

      query = u.merge(query, {
        selections: query.selections || summarySelections,
        joinParams: query.joinParams || summaryJoinParams,
        page: parseInt(page || 1),
        pageSize: parseInt(pageSize || 0) || null,
        orderBy: (orderBy || []).map((item) => u.merge(item, {field: fieldStr(item.field, alias)}))
      });

      query = u.filterOutNulls(query);

      return HttpClient.request({
        url: resourceUri,
        method: 'get',
        params: !!query ? {query: JSON.stringify(query)} : {},
        transformResponse: [parseJson, ((data) => u.merge(data, {
          content: (data.content || []).map(transformResponse)
        }))]
      }).then(rsp => rsp.data).then(data => {
        const {content, pageSize, totalElementsCount} = data;

        data = u.merge(data, {
          data: content,
          totalDataCount: totalElementsCount,
          totalPageCount: computeTotalPageCount(pageSize, totalElementsCount)
        });

        data = u.except(data, ['content', 'totalElementsCount']);
        console.log('#data >>>>> ', data);
        return data;
      });
    };

    const add = (entity) => {
      return HttpClient.request({
        url: resourceUri,
        method: 'post',
        data: entity,
        transformRequest: [transformRequest, JSON.stringify],
        transformResponse: [parseJson, transformResponse],
        headers
      }).then(rsp => rsp.data);
    };

    const update = (entity) => {

      const {id} = entity;
      u.requireNonNull(id, 'ID is required');

      return HttpClient.request({
        url: resourceUri + '/' + id,
        method: 'patch',
        data: entity,
        transformRequest: [transformRequest, JSON.stringify],
        transformResponse: [parseJson, transformResponse],
        headers
      }).then(rsp => rsp.data);
    };

    const deleteById = (id) => {

      return HttpClient.request({
        url: resourceUri + '/' + id,
        method: 'delete',
        transformResponse: [parseJson, transformResponse]
      }).then(rsp => rsp.data);
    };

    return { findById, findAll, add, update, deleteById };
  };
});

const fieldStr = (field, alias) => {
  return !!alias ? (alias + '.' + field) : field;
};

const computeTotalPageCount = (pageSize, totalDataCount) => {
  return (totalDataCount % pageSize) === 0 ? (totalDataCount / pageSize) : (totalDataCount / pageSize + 1);
};

const transformRequest = (data, headers) => {
  data = u.merge(data, {
    createDate: !!data.createDate ? data.createDate.toJSON() : null,
    updateDate: !!data.updateDate ? data.updateDate.toJSON() : null
  });
  return u.filterOutNulls(data);
};

const transformResponse = (data) => {
  data = u.merge(data, {
    createDate: !!data.createDate ? new Date(data.createDate) : null,
    updateDate: !!data.updateDate ? new Date(data.updateDate) : null
  });
  return u.filterOutNulls(data);
};

const parseJson = (jsonStr) => {
  return (jsonStr && JSON.parse(jsonStr));
};
