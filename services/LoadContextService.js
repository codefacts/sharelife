import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('LoadContextService',
['AuthService', 'UserService', 'ErrorCodes'],
({AuthService, UserService, ErrorCodes}) => {
  return () => {
    return AuthService.isAuthenticated()
      .then(isAuth => {
        if (!isAuth) {
          var ex = new Error('Authorization error. Please login.');
          ex.code = ErrorCodes.unauthorized;
          return Promise.reject(ex);
        }
        return AuthService.getUser();
      })
      .then(({userId}) => {
        return UserService.findById(userId);
      });
  };
});
