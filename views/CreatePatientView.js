import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, TouchableHighlight } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('CreatePatientView',
[],
() => {

  const CreatePatientView = (props) => {

    const {name} = props;
    const {onSubmit, onSkip, onChange} = props;

    return (
      <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

        <Text>What is your Patient Name?</Text>

        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          value={name}
          onChangeText={(text) => onChange({name: 'name', value: text})}
        />

        <Button
          title="Submit"
          color="#841584"
          onPress={() => onSubmit({name})}
        />

        {
          !onSkip ? null : (
            <Button
              title="I don't Know"
              color="#841584"
              onPress={onSkip}
            />
          )
        }

      </View>
    );
  };

  return CreatePatientView;
});
