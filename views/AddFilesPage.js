import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, Image, Modal, StyleSheet, ActivityIndicator } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'AddFilesPage',
['EditableFileListView'],
({EditableFileListView}) => {

  class AddFilesPage extends React.Component {

    render () {
      const props = this.props;

      const {showLoader, loaderText} = props;
      const {onAddFile, onNext, onEdit, onDelete} = props;
      const {files, showModal, onModalClose} = props;
      const {showSharingModal, onSharingModalClose} = props;
      const {showSearchModal, onSearchModalClose} = props;
      const {showCreateUserModal} = props;

      const ModalContent = props.ModalContent || (() => null);
      const SharingModalContent = props.SharingModalContent || (() => null);
      const SearchModalContent = props.SearchModalContent || (() => null);
      const CreateUserModalContent = props.CreateUserModalContent || (() => null);

      return (
         <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

           <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>

            <View style={{flex: 1, flexDirection:'column', justifyContent: 'flex-start', alignItems: 'stretch'}}>
              <Button
                title="Add File"
                color="#841584"
                onPress={onAddFile}
              />
            </View>

            <View style={{flex: 1, flexDirection:'column', justifyContent: 'flex-start', alignItems: 'stretch'}}>
              <Button
                title="Next"
                color="#841584"
                onPress={onNext}
              />
            </View>

           </View>

           <View style={{justifyContent: 'flex-start', alignItems: 'stretch'}}>
            <EditableFileListView files={files} onEdit={onEdit} onDelete={onDelete}/>
           </View>

           <Modal
             animationType="slide"
             transparent={false}
             visible={showModal || false}
             onRequestClose={onModalClose}>
             <ModalContent/>
           </Modal>

           <Modal
             animationType="slide"
             transparent={false}
             visible={showSharingModal || false}
             onRequestClose={onSharingModalClose}>
             <SharingModalContent/>
           </Modal>

           <Modal
             animationType="slide"
             transparent={false}
             visible={showSearchModal || false}
             onRequestClose={onSearchModalClose}>
             <SearchModalContent/>
           </Modal>

           <Modal
             animationType="slide"
             transparent={false}
             visible={showCreateUserModal || false}
             onRequestClose={null}>
             <CreateUserModalContent/>
           </Modal>

           <Modal
             animationType="slide"
             transparent={false}
             visible={showLoader || false}
             onRequestClose={null}>
             <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
               <Text>{loaderText}</Text>
               <ActivityIndicator size="large" color="#0000ff" />
             </View>
           </Modal>

         </View>
      );
    }
  };

  return AddFilesPage;
});
