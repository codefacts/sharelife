import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';
import {
  StackNavigator,
} from 'react-navigation';

composer.addAndReturnFactory('AppRouter',
[
  'StatefulProxyViewBuilder', 'AddFilesPageBuilder', 'HistoryPageBuilder', 'HomePageBuilder', 'MyProfilePageBuilder',
  'PasswordInputPageBuilder', 'PeoplePageBuilder', 'PhoneInputPageBuilder', 'RegisterPageBuilder'
],
({
  StatefulProxyViewBuilder, AddFilesPageBuilder, HistoryPageBuilder, HomePageBuilder, MyProfilePageBuilder,
  PasswordInputPageBuilder, PeoplePageBuilder, PhoneInputPageBuilder, RegisterPageBuilder
}) => {

  const AppRouter = StackNavigator(
    {
      AddFilesPage: {screen: AddFilesPageBuilder()},
      HistoryPage: {screen: HistoryPageBuilder()},
      HomePage: {screen: HomePageBuilder()},
      MyProfilePage: {screen: MyProfilePageBuilder()},
      PasswordInputPage: {screen: PasswordInputPageBuilder()},
      PeoplePage: {screen: PeoplePageBuilder()},
      PhoneInputPage: {screen: PhoneInputPageBuilder()},
      RegisterPage: {screen: RegisterPageBuilder()}
    },
    {
      initialRouteName: 'HomePage'
    }
  );

  return AppRouter;
});
