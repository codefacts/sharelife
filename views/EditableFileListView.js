import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, TouchableHighlight } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'EditableFileListView',
['FileItemView'],
({FileItemView}) => {

  const _keyExtractor = (item, index) => item.id;

  return (props) => {

    const files = props.files || [];
    const {onEdit, onDelete} = props;

    const _renderItem = ({item}) => (
      <FileItemView {...item} onEdit={onEdit} onDelete={onDelete}/>
    );

    return (
       <FlatList
         data={files}
         keyExtractor={_keyExtractor}
         renderItem={_renderItem}
       />
    );
  };
});
