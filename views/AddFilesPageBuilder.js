import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, Alert } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';
import Rx from 'rxjs';

composer.addAndReturnFactory('AddFilesPageBuilder',
[
  'StatefulProxyViewBuilder', 'AddFilesPage', 'Navigation', 'AddNewFileView', 'JavaModule', 'CatchError', 'ShareWithPhone', 'PhoneSearchView',
  'UserService', 'LoadContextService', 'RecordService', 'CreatePatientView', 'CreateDoctorView', 'Helpers'
],
({
  StatefulProxyViewBuilder, AddFilesPage, Navigation, AddNewFileView, JavaModule, CatchError, ShareWithPhone, PhoneSearchView,
  UserService, LoadContextService, RecordService, CreatePatientView, CreateDoctorView, Helpers
}) => {

  const {openGallery, takePicture} = JavaModule;

  const handleAddNewFile = (Proxy, files, file) => {

    const isUpdate = !!file;
    file = file || {};

    const onSetPicture = () => {
      ModalProxy.setState({showModal: true});
    };

    const onNameChange = (name) => {
      ModalProxy.setState({name});
    };

    const onDescriptionChange = (description) => {
      ModalProxy.setState({description});
    };

    const onAddFile = ({pictureUri, name, description}) => {
      if (isUpdate) {
        const index = files.findIndex(ff => ff.id === file.id);
        files[index] = u.mergeWithNull(file, {pictureUri, name, description});
        Proxy.updateState({files: files.slice(), showModal: false});
        return;
      }
      const id = newId();
      files.push({id, pictureUri, name, description});
      Proxy.updateState({files: files.slice(), showModal: false});
    };

    const onCancel = () => {
      Proxy.setState({showModal: false, ModalContent: () => null});
    };

    const onModalClose = () => {
      ModalProxy.setState({showModal: false});
    };

    const onOpenCamera = () => {
      ModalProxy.setState({showModal: false});
      takePicture(createImageFileName())
        .then(pictureUri => {
          ModalProxy.setState({pictureUri});
        })
        .catch(CatchError);
    };

    const onOpenGallery = () => {
      ModalProxy.setState({showModal: false});
      openGallery()
      .then(pictureUri => {
        ModalProxy.setState({pictureUri});
      })
      .catch(CatchError);
    };

    const ModalProxy = StatefulProxyViewBuilder(AddNewFileView);

    const ModalContent = (props) => (
      <ModalProxy.ProxyView
        onSetPicture={onSetPicture}
        onNameChange={onNameChange}
        onDescriptionChange={onDescriptionChange}
        onAddFile={onAddFile}
        onCancel={onCancel}
        onModalClose={onModalClose}
        onOpenCamera={onOpenCamera}
        onOpenGallery={onOpenGallery}
        pictureUri={file.pictureUri}
        name={file.name}
        description={file.description}
        submitButtonText={isUpdate ? 'Update File' : 'Add File'}
        {...props}
      />
    );

    return ModalContent;
  };

  const searchByPhone = (phone) => {
    const criteria = {
      op: 'like',
      arg1: {
        op: 'field',
        arg: 'r.phone'
      },
      arg2: (phone + '%')
    };
    const orderBy = [{field: 'phone', order: 'asc'}];
    return UserService.findAll({criteria, orderBy}).then(page => page.data);
  };

  const handleCreateNewUser = ({phone, userType, onSuccess}) => {

    // u.requireNonNull(phone);
    // u.requireNonNull(userType);

    const onChange = ({name, value}) => ModalProxy.setState({[name]: value});

    const onSkip = () => {
      onSuccess(null);
    };

    const onSubmit = (userData) => {
      const {firstName, lastName} = Helpers.splitNames(userData.name);
      onSuccess(u.merge(userData, {firstName, lastName}));
    };

    const UserView = userType === 'patient' ? ((props) => (
      <CreateDoctorView
        onChange={onChange}
        onSubmit={onSubmit}
        onSkip={onSkip}
        {...props}
      />
    )) : ((props) => (
      <CreatePatientView
        onChange={onChange}
        onSubmit={onSubmit}
        onSkip={onSkip}
        {...props}
      />
    ));
    const ModalProxy = StatefulProxyViewBuilder(UserView);

    return ModalProxy.ProxyView;
  };

  const handleCreateRecord = ({Proxy, user, sharingToUser, files, onRecordCreationFinish}) => {

    Proxy.setState({showLoader: true, loaderText: 'Creating Record, please wait...'});

    const includeOnlyFields = ['firstName', 'lastName', 'phone', 'userType', 'isRegistered', 'specialization', 'chamber'];

    const sharingPerson = sharingToUser.isNew ? u.includeOnly(u.merge(sharingToUser, {isRegistered: false}), includeOnlyFields) : {id: sharingToUser.id};

    const userInfo = user.userType === 'patient' ? {patient: {id: user.id}, doctor: sharingPerson} : {patient: sharingPerson, doctor: {id: user.id}};

    RecordService
      .add(
        u.merge({
            files: files.map((file) => u.except(file, ['id'])),
            filesCount: (files || []).length,
            recordDate: new Date().toJSON()
          },
          userInfo
        )
      )
      .then(() => {
        Proxy.setState({showLoader: false, loaderText: null});
        Alert.alert('Success', 'Record created successfully');
        onRecordCreationFinish();
      })
      .catch(ex => {
        Proxy.setState({showLoader: false, loaderText: null});
        return Promise.reject(ex);
      })
      .catch(CatchError);
  };

  const handleSearch = ({phone, user, Proxy, files, onRecordCreationFinish}) => {

    const ModalProxy = StatefulProxyViewBuilder(PhoneSearchView);

    const rspSbj = new Rx.Subject();

    const reqFilter = (() => {
      var prevCounter = -1;
      return ({counter, searchResults}) => {
        const pass = counter > prevCounter;
        prevCounter = counter;
        return pass;
      };
    })();

    rspSbj
      .filter(reqFilter)
      .subscribe({
        next: ({counter, searchResults}) => {
          searchResults = (searchResults.length === 0 && !!phone && !!phone.trim()) ? [createNewUser(phone, user.userType)] : searchResults;
          ModalProxy.setState({searchResults});
        }
      });

    const sbj = new Rx.Subject();

    var req_counter = 1;

    sbj
      .distinctUntilChanged()
      .throttleTime(200)
      .subscribe({
        next: (phone) => {
          const rq_cntr = req_counter++;
          if (!phone) {
            rspSbj.next({counter: rq_cntr, searchResults: []});
          } else {
            searchByPhone(phone).then(searchResults => rspSbj.next({counter: rq_cntr, searchResults}));
          }
        },
        error: (ex) => {
          console.error('ex ' + ex.message);
        },
      });

    sbj.next(phone);

    const onPhoneChange = (_phone) => {
      phone = _phone;
      sbj.next(phone);
      ModalProxy.setState({phone});
    };

    const onSelect = (sharingToUser) => {

      if (!sharingToUser.phone) {
        Alert.alert('', 'No phone number is given. Please type a phone number.');
        return;
      }

      if (sharingToUser.phone.length < 11) {
        Alert.alert('', 'Phone number must be 11 digit. You have ' + (11 - sharingToUser.phone.length) + ' digits missing.');
        return;
      }

      if (sharingToUser.phone.length > 11) {
        Alert.alert('', 'Phone number must be 11 digit. You have ' + (sharingToUser.phone.length - 11) + ' digits extra.');
        return;
      }

      if (!!sharingToUser.isNew) {

        const CreateUserModalContent = handleCreateNewUser({
          phone: sharingToUser.phone,
          userType: user.userType,
          onSuccess: (newUser) => {
            handleCreateRecord({Proxy, user, sharingToUser: u.merge(sharingToUser, newUser, {isNew: true}), files, onRecordCreationFinish});
            Proxy.setState({showCreateUserModal: false, CreateUserModalContent: null});
          }
        });
        Proxy.setState({showCreateUserModal: true, CreateUserModalContent});
        return;
      }

      handleCreateRecord({Proxy, user, sharingToUser, files, onRecordCreationFinish});
    };

    const SearchModalContent = (props) => (
      <ModalProxy.ProxyView
        title="Please type the phone number who you want to share with all the files"
        phone={phone}
        searchResults={[]}
        onPhoneChange={onPhoneChange}
        onSelect={onSelect}
        {...props}
      />
    );
    return SearchModalContent;
  };

  const onContextReady = (Proxy, user) => {

    var files = [];

    const onEdit = (id) => {
      const idx = files.findIndex(ff => ff.id === id);
      const ModalContent = handleAddNewFile(Proxy, files, files[idx]);

      Proxy.setState({
        showModal: true,
        ModalContent
      });
    };

    const onDelete = (id) => {

      const onConfirmDelete = () => {
        files = files.filter((item) => (item.id !== id));
        Proxy.setState({files});
      };

      const onCancelDelete = () => {
      };

      Alert.alert('Delete Confirmation', 'Do you really want to delete?', [{text: 'Delete', onPress: onConfirmDelete}, {text: 'Cancel', onPress: onCancelDelete}], {cancelable: false})
    };

    const onAddNewFile = () => {

      const ModalContent = handleAddNewFile(Proxy, files);

      Proxy.setState({
        showModal: true,
        ModalContent
      });
    };

    const onModalClose = () => {
      Proxy.setState({showModal: false});
    };

    const onNext = () => {

      if (!files || !files.length) {
        Alert.alert('', 'No file added. Please add a file');
        return;
      }

      const onSearchModalClose = () => {
        Proxy.setState({SearchModalContent: (() => null), showSearchModal: false});
      };

      const onRecordCreationFinish = () => {
        files = [];
        Proxy.setState({
          showLoader: false, loaderText: '',
          files,
          showModal: false, ModalContent: null,
          showSharingModal: false, SharingModalContent: null,
          showSearchModal: false, SearchModalContent: null
        });
      };

      const SearchModalContent = handleSearch({phone: '', user, Proxy, files, onRecordCreationFinish});

      Proxy.setState({SearchModalContent, showSearchModal: true, onSearchModalClose});
    };

    Proxy.setState({
      onAddFile: onAddNewFile,
      onNext, onEdit, onDelete, onModalClose, files
    });
  };

  return () => {

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const componentDidMount = () => {
      LoadContextService()
        .then(user => onContextReady(Proxy, user))
        .catch(CatchError);
    };

    const Proxy = StatefulProxyViewBuilder(AddFilesPage, {interceptProps, componentDidMount});

    Proxy.ProxyView.navigationOptions = {
      title: 'Add Files'
    };

    return Proxy.ProxyView;
  };
});

const createImageFileName = () => {
  var dd = new Date().toJSON();
  dd = replaceAll(dd, ':', '-');
  dd = replaceAll(dd, '.', '-');
  return dd;
};

const replaceAll = (target, search, replacement) => {
    return target.split(search).join(replacement);
};

const newId = () => {
  return Math.random() + '-' + Math.random() + '-' + Math.random();
};

const createNewUser = (phone, userType) => {
  return {
    id: newId(), phone: phone, isNew: true, userType: (userType === 'doctor' ? 'patient' : 'doctor')
  };
};
