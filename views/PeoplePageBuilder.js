import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('PeoplePageBuilder',
['StatefulProxyViewBuilder', 'PeoplePage', 'Navigation'],
({StatefulProxyViewBuilder, PeoplePage, Navigation}) => {

  return () => {

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const Proxy = StatefulProxyViewBuilder(PeoplePage, {interceptProps});
    Proxy.ProxyView.navigationOptions = {
      title: 'People'
    };
    return Proxy.ProxyView;
  };
});
