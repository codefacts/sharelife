import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, ActivityIndicator, Image } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'HomePage',
[],
() => {
  return (props) => {
    const navigate = props.navigation.navigate;
    const {showActivityIndicator} = props;
    const {onLogout} = props;

    const mainView = (
      <View style={!!showActivityIndicator ? {flex: 0, width: 0, height: 0} : {flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

        <Button
          title="Add Files"
          color="#841584"
          onPress={() => navigate('AddFilesPage')}
        />

        <Button
          title="My History"
          color="#841584"
          onPress={() => navigate('HistoryPage')}
        />

        <Button
          title="People"
          color="#841584"
          onPress={() => navigate('PeoplePage')}
        />

        <Button
          title="My Profile"
          color="#841584"
          onPress={() => navigate('MyProfilePage')}
        />

        <Button
          title="Log Out"
          color="#841584"
          onPress={onLogout}
        />

      </View>
    );

    const activityView = (
      <View style={!!showActivityIndicator ? {flex: 1, justifyContent: 'center', alignItems: 'center'} : {flex: 0, width: 0, height: 0}}>
        <Text style={{marginBottom: 5}}>Loading data, please wait...</Text>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );

    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>
         {!!showActivityIndicator ? activityView : mainView}
       </View>
    );
  };
});
