import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'ShareWithPhone',
[],
() => {
  return (props) => {
    const {phone} = props;
    const {onFocusPhoneInput, onPhoneChange, onSubmit, onCancel} = props;
    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>

        <Text>Please type in the phone number who you want to share with all the files.</Text>

        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          phone={phone || ''}
          onFocus={onFocusPhoneInput}
          onChangeText={onPhoneChange}
          keyboardType="numeric"
        />

        <Button
          title="Share"
          color="#841584"
          onPress={onSubmit}
        />

        <Button
          title="Cancel"
          color="#841584"
          onPress={onCancel}
        />

       </View>
    );
  };
});
