import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('PeopleItemView',
['HistoryPageHelpers'],
({HistoryPageHelpers}) => {

  const {formatDateTime} = HistoryPageHelpers;

  const formatTime = (timeAsDate) => {
    return timeAsDate.toJSON();
  };

  const PeopleItemView = ({id, pictureUri, name, phone, specialization, chamber, recordsCount, filesCount, time}) => {
    return (
      <View style={{flexDirection:"row", justifyContent: 'flex-start', alignItems: 'flex-start'}}>
        <Image
          style={{flex: 1, width: 50, height: 50}}
          source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
        />
        <View style={{flex: 4, justifyContent: 'flex-start', alignItems: 'stretch'}}>
          <Text>{name}</Text>
          <Text>{phone}</Text>
          {!specialization ? null : (
            <Text>{specialization}</Text>
          )}
          {!chamber ? null : (
            <Text>{chamber}</Text>
          )}
          <Text>{recordsCount} Records | {filesCount} Files</Text>
          <Text>Last Seen: {formatDateTime(time)}</Text>
        </View>
      </View>
    );
  };

  return PeopleItemView;
});
