import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, Image, Modal, StyleSheet } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'OpenImageDialog',
[],
() => {

  return (props) => {

    const {showModal} = props;
    const {onModalClose, onOpenCamera, onOpenGallery} = props;

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={showModal || false}
        onRequestClose={onModalClose}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>
          <Button
            title="Open Camera"
            color="#841584"
            onPress={onOpenCamera}
          />
          <Button
            title="Open Gallery"
            color="#841584"
            onPress={onOpenGallery}
          />
        </View>
      </Modal>
    );
  };
});
