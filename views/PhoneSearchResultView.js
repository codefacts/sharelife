import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, Modal, StyleSheet } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'PhoneSearchResultView',
['SearchResultItemView'],
(factoryConfig) => {

  const _keyExtractor = (item, index) => item.id;

  return (props) => {

    const searchResults = props.searchResults || [];
    const {onSelect} = props;

    const SearchResultItemView = props.SearchResultItemView || factoryConfig.SearchResultItemView;

    const _renderItem = ({item}) => (
      <SearchResultItemView
        {...u.mergeWithNull(item, u.filterOutNulls({onSelect: !onSelect ? null : (() => onSelect(item))}))}
      />
    );

    return (
       <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

         <FlatList
           data={searchResults}
           keyExtractor={_keyExtractor}
           renderItem={_renderItem}
         />

       </View>
    );
  };
});
