import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, TouchableHighlight } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('FileItemView',
[],
() => {

  const FileItemView = (props) => {
    const {id, pictureUri, name, description} = props;
    const {onEdit, onDelete, onPressFileName, onPressImage} = props;
    return (
      <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
        <TouchableHighlight onPress={onPressImage} style={{flex: 1}}>
          <Image
            style={{flex: 1, width: 50, height: 50}}
            source={{uri: pictureUri}}
          />
        </TouchableHighlight>
        <TouchableHighlight onPress={onPressFileName} style={{flex: 4}}>
          <Text>{name}</Text>
        </TouchableHighlight>
        {
          !onEdit ? null : (
            <TouchableHighlight onPress={() => onEdit(id)}>
              <Image
                style={{width: 20, height: 20, marginRight: 15}}
                source={{uri: 'http://www.pvhc.net/img200/bhtbutnquzsbrcyrfupv.png'}}
              />
            </TouchableHighlight>
          )
        }
        {
          !onDelete ? null : (
            <TouchableHighlight onPress={() => onDelete(id)}>
              <Image
                style={{width: 20, height: 20}}
                source={{uri: 'https://cdn2.iconfinder.com/data/icons/e-business-helper/240/627249-delete3-512.png'}}
              />
            </TouchableHighlight>
          )
        }
      </View>
    );
  };
  return FileItemView;
});
