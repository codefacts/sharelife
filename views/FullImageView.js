import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, TouchableHighlight, ScrollView } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('FullImageView',
[],
() => {

  const FullImageView = (props) => {
    const {pictureUri} = props;
    u.requireNonNull(pictureUri, 'pictureUri is required');
    return (
      <ScrollView contentContainerStyle={{height: 900}}>
        <ScrollView horizontal={true} contentContainerStyle={{width: 900}}>
          <Image
            style={{width: 900, height: 900}}
            source={{uri: pictureUri}}
          />
        </ScrollView>
      </ScrollView>
    );
  };
  return FullImageView;
});
