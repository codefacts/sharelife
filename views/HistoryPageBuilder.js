import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('HistoryPageBuilder',
[
  'StatefulProxyViewBuilder', 'HistoryPage', 'Navigation', 'RecordDetailsView', 'FileDetailsView', 'FullImageView',
  'LoadContextService', 'CatchError', 'RecordService', 'UserService', 'CreateDoctorView', 'CreatePatientView', 'Helpers'
],
({
  StatefulProxyViewBuilder, HistoryPage, Navigation, RecordDetailsView, FileDetailsView, FullImageView,
  LoadContextService, CatchError, RecordService, UserService, CreateDoctorView, CreatePatientView, Helpers
}) => {

  const persionSelectionFields = [
    'id',
    'firstName',
    'lastName',
    'specialization',
    'chamber',
    'phone',
    'userType',
    'pictureUri'
  ];

  const summarySelections = [
    'r.id',
    'r.filesCount',
    'r.recordDate',
  ];

  return () => {

    const handleEditUser = ({user, records}) => {

      const onChange = ({name, value}) => ModalProxy.setState({[name]: value});

      const onSubmit = (nUser) => {
        const {firstName, lastName} = Helpers.splitNames(nUser.name);
        const newUser = u.merge(user, nUser, {firstName, lastName});

        const _newUser = () => u.merge({sharingPerson: newUser}, user.userType === 'patient' ? {doctor: newUser} : {patient: newUser});

        UserService
          .add(newUser)
          .then(() => {
            records = records.map((item) => u.merge(item, item.sharingPerson.id === newUser.id ? _newUser() : {sharingPerson: item.sharingPerson}))
            Proxy.setState({records, EditUserModalContent: null});
          })
          .catch(CatchError);
      };

      const ModalProxy = StatefulProxyViewBuilder(user.userType === 'patient' ? CreatePatientView : CreateDoctorView);

      const EditUserModalContent = (props) => (
        <ModalProxy.ProxyView
          name={(user.firstName + (!user.lastName ? '' : (' ' + user.lastName)) || '')}
          onChange={onChange}
          onSubmit={onSubmit}
          {...u.mergeWithNull(props, u.filterOutNulls(u.includeOnly(user, ['specialization', 'chamber'])))}
        />
      );

      Proxy.setState({EditUserModalContent});
    };

    const onPressFileDetails = (file) => {
      const FileDetailsModelContent = () => {
        return (
          <FileDetailsView
            {...file}
          />
        );
      };
      Proxy.setState({FileDetailsModelContent});
    };

    const onPressImage = (item) => {
      // alert('item: ' + JSON.stringify(item));
      const FullImageModelContent = (props) => {
        return (
          <FullImageView
            pictureUri={item.pictureUri}
          />
        );
      };
      Proxy.setState({FullImageModelContent});
    };

    const handlePressRecord = ({user, record}) => {

      Proxy.setState({showLoader: true, loaderText: 'Loading data, please wait...'});

      RecordService.findById(record.id)
        .then(fullRecord => {

          const RecordDetailsModelContent = (props) => {
            return (
              <RecordDetailsView
                {...u.merge(fullRecord, user.userType === 'patient' ? {sharingPerson: fullRecord.doctor} : {sharingPerson: fullRecord.patient})}
                onPressFileDetails={onPressFileDetails}
                onPressImage={onPressImage}
              />
            );
          };
          Proxy.setState({RecordDetailsModelContent, showLoader: false, loaderText: null});
        })
        .catch(ex => {
          Proxy.setState({showLoader: false, loaderText: null});
          return Promise.reject(ex);
        })
        .catch(CatchError);
    };

    const onContextReady = ({user, records}) => {

      Proxy.setState({
        records,
        onEditUser: (nUser) => handleEditUser({user: nUser, records}),
        onPressRecord: (record) => handlePressRecord({user, record})
      });
    };

    const componentDidMount = () => {
      LoadContextService()
        .then(user => {

          const conditionalField = user.userType === 'patient' ? 'r.patient.id' : 'r.doctor.id';

          const criteria = {
            op: 'eq',
            arg1: {
              op: 'field',
              arg: conditionalField
            },
            arg2: user.id
          };

          const selectPrefix = user.userType === 'patient' ? 'r.doctor.' : 'r.patient.';

          const selections = summarySelections.concat(
            persionSelectionFields.map((field) => selectPrefix + field)
          );

          const orderBy = [{field: 'recordDate', order: 'desc'}];

          const mapRecords = (records) => records.map((item) => u.merge(item, {sharingPerson: user.userType === 'patient' ? item.doctor : item.patient}));

          return RecordService.findAll({criteria, selections, orderBy})
            .then(pg => ({user, records: mapRecords(pg.data)}));
        })
        .then(onContextReady)
        .catch(CatchError);
    };

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const Proxy = StatefulProxyViewBuilder(HistoryPage, {interceptProps, componentDidMount});

    const onRecordDetailsModalClose = () => Proxy.setState({RecordDetailsModelContent: null});
    const onFileDetailsModalClose = () => Proxy.setState({FileDetailsModelContent: null});
    const onFullImageModalClose = () => Proxy.setState({FullImageModelContent: null});
    const onEditUserModalClose = () => Proxy.setState({EditUserModalContent: null});

    const _HistoryPage = (props) => (
      <Proxy.ProxyView
        onPressFileDetails={onPressFileDetails}
        onRecordDetailsModalClose={onRecordDetailsModalClose}
        onFileDetailsModalClose={onFileDetailsModalClose}
        onFullImageModalClose={onFullImageModalClose}
        onEditUserModalClose={onEditUserModalClose}
        {...props}
      />
    );

    _HistoryPage.navigationOptions = {
      title: 'My History'
    };

    return _HistoryPage;
  };
});

const randomId = () => (Math.random() + '-' + Math.random() + '-' + Math.random());
