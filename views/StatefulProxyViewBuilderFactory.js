import React, {Component} from 'react';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

export default composer.addAndReturnFactory('StatefulProxyViewBuilder',
['StatefulProxyView', 'EventBus'],
({StatefulProxyView, EventBus}) => {

  return (ProxyView, config) => {

    u.requireNonNull(ProxyView, 'ProxyView is required');

    const {componentDidUpdate, componentDidMount, componentWillUnmount, interceptProps} = u.mergeWithNull({
      componentDidUpdate: (prevProps, prevState) => null,
      componentDidMount: (props, state) => null,
      componentWillUnmount: (props, state) => null,
      interceptProps: (props) => null
    }, config);

    const $e_address = 'proxy-view-' + Math.random() + '-' + Math.random() + '-' + Math.random();

    var _state = {};

    const setState = (newState) => EventBus.emit($e_address, _state = (newState || {}));

    const updateState = (newState) => {
      EventBus.emit($e_address, _state = u.mergeWithNull(_state, newState));
    };

    const __proxyTo = (props) => (<ProxyView {...props}/>);

    return {
      ProxyView: (props) => {
        return (
          <StatefulProxyView
            $e_address={$e_address}
            __proxyTo={__proxyTo}
            __interceptProps={interceptProps}
            __componentDidUpdate={componentDidUpdate}
            __componentDidMount={componentDidMount}
            __componentWillUnmount={componentWillUnmount}
            {...props}
            />
        );
      },
      setState: setState,
      updateState: updateState
    };
  };
});
