import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, Modal, ActivityIndicator } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('HistoryPage',
['RecordItemView', 'HistoryPageHelpers'],
({RecordItemView, HistoryPageHelpers}) => {

  const {toPeriodDisplayText} = HistoryPageHelpers;

  const TimeItem = ({timePeriodInMillis}) => {
    return (
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text style={{flex: 1}}>{toPeriodDisplayText(timePeriodInMillis)}</Text>
      </View>
    );
  };

  const _keyExtractor = (item, index) => item.id;

  return (props) => {

    const records = props.records || [];
    const {onPressRecord, onEditUser} = props;

    const {onRecordDetailsModalClose, RecordDetailsModelContent} = props;
    const {onFileDetailsModalClose, FileDetailsModelContent} = props;
    const {onFullImageModalClose, FullImageModelContent} = props;
    const {onEditUserModalClose, EditUserModalContent} = props;

    const {showLoader, loaderText} = props;

    const _renderItem = ({item}) => {
      if (item._itemType === 'time') {
        return (
          <TimeItem timePeriodInMillis={item.timePeriodInMillis}/>
        );
      } else {
        return (
          <RecordItemView {...item} onPressRecord={() => onPressRecord(item)} onEditUser={() => onEditUser(item.sharingPerson)}/>
        );
      }
    };

    return (
       <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

         <FlatList
           data={records}
           keyExtractor={_keyExtractor}
           renderItem={_renderItem}
         />

         <Modal
           animationType="slide"
           transparent={false}
           visible={!!RecordDetailsModelContent}
           onRequestClose={onRecordDetailsModalClose}
         >
          {!RecordDetailsModelContent ? (<EMPTY_VIEW/>) : (<RecordDetailsModelContent/>)}
         </Modal>

         <Modal
           animationType="slide"
           transparent={false}
           visible={!!FileDetailsModelContent}
           onRequestClose={onFileDetailsModalClose}
         >
          {!FileDetailsModelContent ? (<EMPTY_VIEW/>) : (<FileDetailsModelContent/>)}
         </Modal>

         <Modal
           animationType="slide"
           transparent={false}
           visible={!!FullImageModelContent}
           onRequestClose={onFullImageModalClose}
         >
          {!FullImageModelContent ? (<EMPTY_VIEW/>) : (<FullImageModelContent/>)}
         </Modal>

         <Modal
           animationType="slide"
           transparent={false}
           visible={!!EditUserModalContent}
           onRequestClose={onEditUserModalClose}
         >
          {!EditUserModalContent ? (<EMPTY_VIEW/>) : (<EditUserModalContent/>)}
         </Modal>


         <Modal
           animationType="slide"
           transparent={false}
           visible={showLoader || false}
           onRequestClose={null}>
           <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
             <Text>{loaderText}</Text>
             <ActivityIndicator size="large" color="#0000ff" />
           </View>
         </Modal>

       </View>
    );
  };
});

const EMPTY_VIEW = () => null;
