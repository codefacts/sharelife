import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'MyProfilePage',
[],
() => {
  return (props) => {
    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>

         <Image
           style={{width: 50, height: 50}}
           source={{uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'}}
         />

         <TextInput
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           keyboardType="numeric"
           value="+8801951883412"
         />

         <TextInput
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value="Shahadat Khan"
         />

         <Button
           title="Update"
           color="#841584"
         />

       </View>
    );
  };
});
