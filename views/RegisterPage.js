import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, Image, Modal, StyleSheet } from 'react-native';
import { RNCamera } from 'react-native-camera';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'RegisterPage',
['OpenImageDialog'],
({OpenImageDialog}) => {

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'black'
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    capture: {
      flex: 0,
      backgroundColor: '#fff',
      borderRadius: 5,
      padding: 15,
      paddingHorizontal: 20,
      alignSelf: 'center',
      margin: 20
    }
  });

  return (props) => {
    const {name, password, password11, pictureUri, showModal} = props;
    const {onNameChange, onPasswordChange, onPassword11Change, onRegister, onSetPicture, onModalClose} = props;
    const {onOpenCamera, onOpenGallery} = props;
    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>

         <Image
           style={{width: 50, height: 50, marginRight: 15}}
           source={{uri: pictureUri}}
         />

         <Button
           title={!pictureUri ? 'Set Your Profile Photo' : 'Change Profile Picture'}
           color="#841584"
           onPress={onSetPicture}
         />

         <Text>Name</Text>
         <TextInput
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={name}
           onChangeText={onNameChange}
         />

         <Text>Password</Text>
         <TextInput
           secureTextEntry={true}
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={password}
           onChangeText={onPasswordChange}
         />

         <Text>Password Again</Text>
         <TextInput
           secureTextEntry={true}
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={password11}
           onChangeText={onPassword11Change}
         />

         <Button
           title="Register"
           color="#841584"
           onPress={() => onRegister({name, password, password11, pictureUri})}
         />

         <OpenImageDialog
           showModal={showModal}
           onModalClose={onModalClose}
           onOpenCamera={onOpenCamera}
           onOpenGallery={onOpenGallery}
         />

       </View>
    );
  };
});
