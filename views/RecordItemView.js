import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, TouchableHighlight } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('RecordItemView',
['HistoryPageHelpers'],
({HistoryPageHelpers}) => {

  const {formatDateTime} = HistoryPageHelpers;

  const RecordItemView = (props) => {

    const {id, filesCount, recordDate} = props;
    const {pictureUri, firstName, lastName, phone, specialization, chamber} = props.sharingPerson || {};
    const {onPressRecord, onEditUser} = props;

    return (
      <View style={{flexDirection:"row", justifyContent: 'flex-start', alignItems: 'flex-start'}}>
        <Image
          style={{flex: 4, width: 50, height: 50}}
          source={{uri: pictureUri || 'http://192.168.100.6:1582/public/images/foods.png'}}
        />
        <View style={{flex: 20, justifyContent: 'flex-start', alignItems: 'stretch'}}>
          <Text>{firstName + (!lastName ? '' : (' ' + lastName))}</Text>
          <Text>{phone}</Text>
          {
            !specialization ? null : (
              <Text>{specialization}</Text>
            )
          }
          <Text>{filesCount} Files</Text>
          <Text>{formatDateTime(recordDate)}</Text>
        </View>
        <View style={{flex: 4, justifyContent: 'flex-start', alignItems: 'stretch'}}>
          <Button
            title="Edit"
            color="#841584"
            onPress={onEditUser}
          />
          <Button
            title="View"
            color="#841584"
            onPress={onPressRecord}
          />
        </View>
      </View>
    );
  };

  return RecordItemView;
});
