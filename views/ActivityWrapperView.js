import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, ActivityIndicator } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'ActivityWrapperView',
[],
() => {

  return (props) => {

    const {_Content, _message, _showActivityIndicator} = props;
    u.requireNonNull(_Content, 'Content is required');

    if (!_showActivityIndicator) {
      return <_Content {...u.except(props, ['_Content', '_message', '_showActivityIndicator'])}/>;
    }

    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{marginBottom: 5}}>Loading data, please wait...</Text>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  };
});
