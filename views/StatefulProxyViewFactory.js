import React, {Component} from 'react';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

export default composer.addAndReturnFactory('StatefulProxyView',
['EventBus'],
(factoryConfig) => {

  const {EventBus} = factoryConfig;

  const except = (props) => u.except(props, [
    '$e_address',
    '__interceptProps',
    '__proxyTo',
    '__componentDidUpdate',
    '__componentDidMount',
    '__componentWillUnmount'
  ]);

  class StatefulProxyView extends Component {

      constructor(props) {
          super(props);
          this.state = {};
          //console.log('StatefulProxyView-Props', except(props));
          props.__interceptProps(except(props));
      }

      componentWillReceiveProps(nextProps) {
        this.props.__interceptProps(except(nextProps));
      }

      componentDidUpdate(prevProps, prevState) {
        this.props.__componentDidUpdate(except(prevProps), prevState);
      }

      componentDidMount() {
        const {$e_address} = this.props;
        u.requireNonNull($e_address);
        EventBus.on($e_address, (newState) => this.setState(newState));
        this.props.__componentDidMount(except(this.props), this.state);
      }

      componentWillUnmount() {
        const {$e_address} = this.props;
        u.requireNonNull($e_address);
        EventBus.removeAllListeners([$e_address]);
        this.props.__componentWillUnmount(except(this.props), this.state);
      }

      render() {

        const {props, state} = this;

        const _props = except(u.mergeWithNull(props, state));

        const {__proxyTo} = props;

        return (
          <__proxyTo {..._props}/>
        );

      }
  }

  StatefulProxyView.defaultProps = {
    __interceptProps: (props) => null,
    __proxyTo: (props) => null,
    __componentDidUpdate: (prevProps, prevState) => null,
    __componentDidMount: (props, state) => null,
    __componentWillUnmount: (props, state) => null
  };

  return StatefulProxyView;
});
