import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, PermissionsAndroid } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('HomePageBuilder',
['StatefulProxyViewBuilder', 'HomePage', 'Navigation', 'LoadContextService', 'CatchError', 'ErrorCodes', 'AuthService'],
({StatefulProxyViewBuilder, HomePage, Navigation, LoadContextService, CatchError, ErrorCodes, AuthService}) => {

  return () => {

    const onReady = (user) => {
      Proxy.setState({showActivityIndicator: false});
    };

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const componentDidMount = () => {
      PermissionsAndroid
        .request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'ShareLife Camera Permission',
            message: 'ShareLife needs access to your camera ' +
                       'so you can take pictures.'
          }
        )
        .then(granted => {
          if (granted != 'granted') {
            return Promise.reject(new Error('Camera Permission denied by the user'));
          }
        })
        .catch(CatchError);

      LoadContextService()
        .then(onReady)
        .catch(ex => {
          if (ex.code == ErrorCodes.unauthorized) {
            Navigation.navigate('PhoneInputPage');
            return;
          }
          return Promise.reject(ex);
        })
        .catch(CatchError);
    };

    const onLogout = () => {
      AuthService.logout().then(() => Navigation.navigate('PhoneInputPage')).catch(CatchError);
    };

    const Proxy = StatefulProxyViewBuilder((props) => (
      <HomePage
        onLogout={onLogout}
        showActivityIndicator={true}
        {...props}
      />
    ), {componentDidMount, interceptProps});

    Proxy.ProxyView.navigationOptions = {
      title: 'ShareLife'
    };

    return Proxy.ProxyView;
  };
});
