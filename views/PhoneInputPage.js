import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'PhoneInputPage',
[],
() => {
  return (props) => {
    const {phone} = props;
    const {onNext, onPhoneChange} = props;
    console.log('phone: ' + phone);
    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>

         <Text>Phone</Text>
         <TextInput
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={phone || ''}
           onChangeText={text => onPhoneChange(text)}
         />

         <Button
           title="Next"
           color="#841584"
           onPress={e => onNext(phone)}
         />

       </View>
    );
  };
});
