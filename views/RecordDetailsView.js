import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('RecordDetailsView',
['FileItemView', 'HistoryPageHelpers'],
({FileItemView, HistoryPageHelpers}) => {

  const {formatDateTime} = HistoryPageHelpers;

  const SharingPersonView = (props) => {
    const {pictureUri, name, phone, specialization, chamber, recordDate} = props;
    return (
      <View style={{flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
        <Image source={{uri: pictureUri}} style={{flex: 2, width: 50, height: 50}}/>
        <View style={{flex: 8, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
          <Text>{name}</Text>
          <Text>{phone}</Text>
          {
            !specialization ? null : (
              <Text>{specialization}</Text>
            )
          }
          {
            !chamber ? null : (
              <Text>{chamber}</Text>
            )
          }
          <Text>{formatDateTime(recordDate)}</Text>
        </View>
      </View>
    );
  };

  const _keyExtractor = (item, index) => item.id;

  return (props) => {

    const {files, recordDate, sharingPerson} = props;

    const {firstName, lastName, pictureUri, phone, specialization, chamber} = sharingPerson;
    const {onPressFileDetails, onPressImage, onFileEdit, onFileDelete} = props;

    const _renderItem = ({item}) => {

      const editorConfig = u.filterOutNulls({
        onEdit: !onFileEdit ? null : onFileEdit(item),
        onDelete: !onFileDelete ? null : onFileDelete(item)
      });

      return (
        <FileItemView
          onPressFileName={() => onPressFileDetails(item)}
          onPressImage={() => onPressImage(item)}
          {...u.merge(item, editorConfig)}
        />
      );
    };

    return (
       <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

          <SharingPersonView
            name={firstName + (!lastName ? '' : (' ' + lastName))}
            pictureUri={pictureUri}
            phone={phone}
            recordDate={recordDate}
            specialization={specialization}
            chamber={chamber}
          />

          <FlatList
           data={files}
           keyExtractor={_keyExtractor}
           renderItem={_renderItem}
          />

       </View>
    );
  };
});
