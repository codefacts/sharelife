import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('MyProfilePageBuilder',
['StatefulProxyViewBuilder', 'MyProfilePage', 'Navigation'],
({StatefulProxyViewBuilder, MyProfilePage, Navigation}) => {

  return () => {

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const Proxy = StatefulProxyViewBuilder(MyProfilePage, {interceptProps});
    Proxy.ProxyView.navigationOptions = {
      title: 'My Profile'
    };
    return Proxy.ProxyView;
  };
});
