import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, TouchableHighlight, ScrollView } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('FileDetailsView',
[],
() => {

  const FileDetailsView = (props) => {
    const {id, pictureUri, name, description} = props;
    const {onEdit, onDelete, onPressImage} = props;
    return (
      <ScrollView style={{flex: 1}} showsHorizontalScrollIndicator={true} showsVerticalScrollIndicator={true}>

        <TouchableHighlight onPress={onPressImage} style={{height: 600, width: 600}}>
          <Image
            style={{flex: 1, height: 600, width: 600}}
            source={{uri: pictureUri}}
          />
        </TouchableHighlight>

        <View style={{alignItems: 'center'}}>
          <Text>{name}</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Text>{description}</Text>
        </View>

        {
          (!onEdit && !onDelete) ? null : (
            <View style={{flexDirection: 'row', justifyContent: 'center', height: 50}}>
              <TouchableHighlight onPress={() => onEdit(id)} style={{marginRight: 20}}>
                <Image
                  style={{height: 50, width: 50}}
                  source={{uri: 'http://www.pvhc.net/img200/bhtbutnquzsbrcyrfupv.png'}}
                />
              </TouchableHighlight>
              <TouchableHighlight onPress={() => onDelete(id)}>
                <Image
                  style={{height: 50, width: 50}}
                  source={{uri: 'https://cdn2.iconfinder.com/data/icons/e-business-helper/240/627249-delete3-512.png'}}
                />
              </TouchableHighlight>
            </View>
          )
        }

      </ScrollView>
    );
  };
  return FileDetailsView;
});
