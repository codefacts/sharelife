import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('PeoplePage',
['PeopleItemView'],
({PeopleItemView}) => {

  const _keyExtractor = (item, index) => item.id;

  const _renderItem = ({item}) => {
    return (
      <PeopleItemView {...item}/>
    );
  };

  return (props) => {

    const items = props.items || [{id: 1, name: 'Konik Khanna', phone: '01951883412', recordsCount: 5, filesCount: 156, time: new Date()}];

    return (
       <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

         <FlatList
           data={items}
           keyExtractor={_keyExtractor}
           renderItem={_renderItem}
         />

       </View>
    );
  };
});
