import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, Image, Modal, StyleSheet } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory(
'PhoneSearchView',
['PhoneSearchResultView'],
({PhoneSearchResultView}) => {
  return (props) => {

    const {searchResults, phone, onPhoneChange, onSelect, title} = props;

    return (
       <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'stretch'}}>

         {
           !title ? null : (
             <Text>
                {title}
             </Text>
           )
         }

         <TextInput
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={phone || ''}
           onChangeText={onPhoneChange}
           autoFocus={true}
           keyboardType="numeric"
         />

         <PhoneSearchResultView
            searchResults={searchResults}
            {...u.mergeWithNull(
              u.filterOutNulls({onSelect})
            )}
         />

       </View>
    );
  };
});
