import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, Image, Modal, StyleSheet } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'SearchResultItemView',
[],
() => {
  return (props) => {

    const {pictureUri, phone, firstName, lastName, specialization, chamber, onSelect} = props;

    return (
       <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>

         <Image source={{uri: pictureUri}} style={{flex: 2, width: 50, height: 50}}/>

         <View style={{flex: 8, justifyContent: 'flex-start', alignItems: 'stretch'}}>

           <Text>{(firstName || '') + (!lastName ? '' : (' ' + lastName))}</Text>

           <Text>{phone}</Text>

           {
             !specialization ? null : (
               <Text>{specialization}</Text>
             )
           }

         </View>

         {
           !onSelect ? null : (
             <Button
               title="Select"
               color="#841584"
               style={{flex: 2}}
               onPress={onSelect}
             />
           )
         }

       </View>
    );
  };
});
