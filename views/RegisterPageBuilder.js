import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, Alert } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('RegisterPageBuilder',
['StatefulProxyViewBuilder', 'RegisterPage', 'Navigation', 'JavaModule', 'CatchError', 'UserService', 'AuthService'],
({StatefulProxyViewBuilder, RegisterPage, Navigation, JavaModule, CatchError, UserService, AuthService}) => {

  const {openGallery, takePicture} = JavaModule;

  const createImageFileName = () => {
    var dd = new Date().toJSON();
    dd = replaceAll(dd, ':', '-');
    dd = replaceAll(dd, '.', '-');
    return dd;
  };

  return () => {

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const onPasswordChange = (password) => {
      Proxy.setState({password});
    };

    const onPassword11Change = (password11) => {
      Proxy.setState({password11});
    };

    const onNameChange = (name) => {
      Proxy.setState({name});
    };

    const onRegister = ({name, password, password11, pictureUri}) => {

      if (password !== password11) {
        Alert.alert('Error', 'Two passwords must be same. Please type same password twice.');
        return;
      }

      const phone = Navigation.params()['phone'];
      u.requireNonNull(phone, 'Phone is required');

      u.requireNonNull(name, 'Name is required');

      const {firstName, lastName} = splitName(name);

      AuthService.login({user: '+8801954312686', password: '651654948516949846565464'})
        .then(() => {
          alert('ANDROID_CLIENT logged in successfully.');
          return UserService
            .add({
              firstName,
              lastName,
              phone,
              password,
              pictureUri,
              isRegistered: true
            });
        })
        .then(() => {
          alert('Registration successul, Please login to continue');
          Navigation.navigate('PhoneInputPage');
        })
        .then(() => AuthService.logout())
        .catch((ex) => {
          AuthService.logout();
          return Promise.reject(ex);
        })
        .catch(CatchError)
        ;
    };

    const onSetPicture = () => {
      Proxy.setState({showModal: true});
    };

    const onModalClose = () => {
      Proxy.setState({showModal: false});
    };

    const onOpenCamera = () => {
      Proxy.setState({showModal: false});
      takePicture(createImageFileName())
        .then(pictureUri => {
          Proxy.setState({pictureUri});
        })
        .catch(CatchError);
    };

    const onOpenGallery = () => {
      Proxy.setState({showModal: false});
      openGallery()
      .then(pictureUri => {
        Proxy.setState({pictureUri});
      })
      .catch(CatchError);
    };

    const Proxy = StatefulProxyViewBuilder((props) => (
      <RegisterPage
        onSetPicture={onSetPicture}
        onModalClose={onModalClose}
        onNameChange={onNameChange}
        onPasswordChange={onPasswordChange}
        onPassword11Change={onPassword11Change}
        onRegister={onRegister}
        onOpenCamera={onOpenCamera}
        onOpenGallery={onOpenGallery}
        {...props}
      />
    ), {interceptProps});

    Proxy.ProxyView.navigationOptions = {
      title: 'Registration'
    };

    return Proxy.ProxyView;
  };
});


const replaceAll = (target, search, replacement) => {
    return target.split(search).join(replacement);
};

const splitName = (name) => {
  const splits = name.split(' ');
  if (splits.length === 1) {
    return {firstName: splits[0], lastName: null};
  }
  const firstName = splits.slice(0, splits.length - 1).join(' ');
  const lastName = splits[splits.length - 1];
  return {firstName, lastName};
};
