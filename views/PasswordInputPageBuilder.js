import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, Alert } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('PasswordInputPageBuilder',
['StatefulProxyViewBuilder', 'PasswordInputPage', 'Navigation', 'AuthService', 'CatchError'],
({StatefulProxyViewBuilder, PasswordInputPage, Navigation, AuthService, CatchError}) => {

  return () => {

    var _password = null;

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const onPasswordChange = (newPassword) => {
      Proxy.setState({password: (_password = newPassword)});
    };

    const onLogin = (password) => {
      const {phone} = Navigation.params();
      AuthService.login({user: phone, password})
        .then(authData => {
          // Alert.alert('', JSON.stringify(authData));
          Navigation.navigate('HomePage', authData);
        })
        .catch(CatchError);
    };

    const Proxy = StatefulProxyViewBuilder((props) => (
      <PasswordInputPage
        password={_password}
        onPasswordChange={onPasswordChange}
        onLogin={onLogin}
        {...props}
      />
    ), {interceptProps});

    Proxy.ProxyView.navigationOptions = {
      title: 'Password Required'
    };

    return Proxy.ProxyView;
  };
});
