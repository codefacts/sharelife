import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button, Alert } from 'react-native';
import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('PhoneInputPageBuilder',
['StatefulProxyViewBuilder', 'PhoneInputPage', 'UserService', 'CatchError', 'Navigation'],
({StatefulProxyViewBuilder, PhoneInputPage, UserService, CatchError, Navigation}) => {

  return () => {

    var _phone = null;

    const _onNext = (phone) => {
      console.log('phone: ' + phone);
      UserService.checkPhoneExists(phone)
        .then(exists => {
          if (exists) {
            Navigation.navigate('PasswordInputPage', {phone});
          } else {
            Navigation.navigate('RegisterPage', {phone});
          }
        })
        .catch(CatchError);
    };

    const _onPhoneChange = (newPhone) => {
      Proxy.setState({phone: (_phone = newPhone)});
    };

    const interceptProps = ({navigation}) => {
      Navigation.setNavigation(navigation);
    };

    const Proxy = StatefulProxyViewBuilder((props) => (
      <PhoneInputPage
        phone={_phone} onNext={_onNext} onPhoneChange={_onPhoneChange}
        {...props}
      />
    ), {interceptProps});

    Proxy.ProxyView.navigationOptions = {
      title: 'Phone Number'
    };

    return Proxy.ProxyView;
  };
});
