import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'PasswordInputPage',
[],
() => {
  return (props) => {
    const {password} = props;
    const {onPasswordChange, onLogin} = props;
    return (
       <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>

         <Text>Password</Text>
         <TextInput
           secureTextEntry={true}
           style={{height: 40, borderColor: 'gray', borderWidth: 1}}
           value={password || ''}
           onChangeText={newPassword => onPasswordChange(newPassword)}
         />

         <Button
           title="Login"
           color="#841584"
           onPress={e => onLogin(password)}
         />

       </View>
    );
  };
});
