import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Button, FlatList, Image, Modal } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory('HistoryPageHelpers',
[],
() => {

  const toPeriodDisplayText = (timePeriodInMillis) => {
    const oneDayMilis = 24 * 60 * 60 * 1000;
    const year = Math.floor(timePeriodInMillis / (365 * oneDayMilis));
    const month = Math.floor(timePeriodInMillis / (30 * oneDayMilis));
    const day = Math.floor(timePeriodInMillis / (oneDayMilis));
    const hour = Math.floor(timePeriodInMillis / (60 * 60 * 1000));
    const minute = Math.floor(timePeriodInMillis / (60 * 1000));
    const second = Math.floor(timePeriodInMillis / 1000);

    if (!!Math.floor(year)) {
      const yy = Math.floor(year);
      const mm = Math.floor((day - (yy * 365)) / 30);
      const dd = Math.floor(day - (yy * 365 + mm * 30));
      return yy + ' Years ' + mm + ' Months ' + dd + ' Days';
    }

    if (!!Math.floor(month)) {
      const mm = Math.floor(month);
      const dd = Math.floor((hour - (30 * month * 24)) / 24);
      const hh = Math.floor(hour - (mm * 30 * 24 + dd * 24));
      return mm + ' Months ' + dd + ' Days ' + hh + ' Hours';
    }

    if (!!Math.floor(day)) {
      const dd = Math.floor(day);
      const hh = Math.floor((minute - dd * 24 * 60) / 24);
      const mm = Math.floor(minute - (dd * 24 * 60 + hh * 60));
      return dd + ' Days ' + hh + ' Hours ' + mm + ' Mins';
    }

    if (!!Math.floor(hour)) {
      const hh = Math.floor(hour);
      const mm = Math.floor((second - hh * 60 * 60) / 60);
      const ss = Math.floor(second - (hh * 60 * 60 + mm * 60));
      return hh + ' Hours ' + mm + ' Mins ' + ss + ' Secs';
    }

    return Math.floor(minute) + ' Mins';
  };

  const formatDateTime = (date) => {
    return date.getDate() + ' (' + dayFullName(date) + ')' + ' / ' + (date.getMonth() + 1) + ' (' + monthName(date) + ')' + ' / ' + date.getFullYear() + ' ' + formatTimeAmPm(date);
  };

  return {
    toPeriodDisplayText,
    formatDateTime
  };
});

const EMPTY_VIEW = () => null;

const DAY_NAMES = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
const MONTH_NAMES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const dayFullName = (date) => {
  return DAY_NAMES[date.getDay()];
};

const monthName = (date) => {
  return MONTH_NAMES[date.getMonth()];
};

const formatTimeAmPm = (date) => {
  const hr = date.getHours();
  const mn = date.getMinutes();
  const am_pm = 0 <= hr && hr < 12 ? 'AM' : 'PM';
  const hrr = hr == 0
    ? 12
    : am_pm === 'AM'
    ? hr
    : hr === 12
    ? hr
    : hr - 12;
  return hrr + ':' + mn + ' ' + am_pm;
};
