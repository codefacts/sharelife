import React, { Component } from 'react';
import { AppRegistry, View, Text, TextInput, Image, Button } from 'react-native';
import composer from '../core/Composer.js';

composer.addAndReturnFactory(
'AddNewFileView',
['OpenImageDialog'],
({OpenImageDialog}) => {

  return (props) => {

    const {pictureUri, name, description} = props;
    const {onSetPicture, onNameChange, onDescriptionChange, onAddFile, onCancel} = props;

    const {showModal, onModalClose, submitButtonText} = props;
    const {onOpenCamera, onOpenGallery} = props;

    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'stretch'}}>

        <Image
          style={{width: 50, height: 50, marginRight: 15}}
          source={{uri: pictureUri}}
        />

        <Button
          title={!pictureUri ? 'Set Picture' : 'Change Picture'}
          color="#841584"
          onPress={onSetPicture}
        />

        <Text>Title</Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          value={name}
          onChangeText={onNameChange}
        />

        <Text>Description</Text>
        <TextInput
          style={{borderColor: 'gray', borderWidth: 1, alignItems:"flex-start", justifyContent:"flex-start"}}
          value={description}
          multiline={true}
          numberOfLines={5}
          onChangeText={onDescriptionChange}
        />

        <Button
          title={submitButtonText || 'Add File'}
          color="#841584"
          onPress={() => onAddFile({name, description, pictureUri})}
        />

        <Button
          title="Cancel"
          color="#841584"
          onPress={onCancel}
        />

        <OpenImageDialog
          showModal={showModal}
          onModalClose={onModalClose}
          onOpenCamera={onOpenCamera}
          onOpenGallery={onOpenGallery}
        />

      </View>
    );
  };
});
