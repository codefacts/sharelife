require('./core/StateStoreFactory.js');
require('./core/EventBus.js');

require('./core/HttpClient.js');
require('./core/LocalStorageFactory.js');

require('./views/HomePage.js');
require('./views/HomePageBuilder.js');
require('./views/PhoneInputPage.js');
require('./views/PhoneInputPageBuilder.js');
require('./views/PasswordInputPage.js');
require('./views/PasswordInputPageBuilder.js');
require('./views/RegisterPage.js');
require('./views/RegisterPageBuilder.js');
require('./views/AddFilesPage.js');
require('./views/AddFilesPageBuilder.js');
require('./views/EditableFileListView.js');
require('./views/ShareWithPhone.js');
require('./views/HistoryPage.js');
require('./views/HistoryPageHelpers.js');
require('./views/HistoryPageBuilder.js');
require('./views/RecordItemView.js');
require('./views/PeopleItemView.js');
require('./views/PeoplePage.js');
require('./views/PeoplePageBuilder.js');
require('./views/MyProfilePage.js');
require('./views/MyProfilePageBuilder.js');
require('./views/AppRouter.js');
require('./views/ActivityWrapperView.js');

require('./views/AddNewFileView.js');
require('./views/OpenImageDialog.js');
require('./views/PhoneSearchView.js');
require('./views/PhoneSearchResultView.js');
require('./views/SearchResultItemView.js');

require('./views/FileItemView.js');
require('./views/RecordDetailsView.js');
require('./views/FileDetailsView.js');
require('./views/FullImageView.js');

require('./views/CreatePatientView.js');
require('./views/CreateDoctorView.js');

require('./views/StatefulProxyViewBuilderFactory.js');
require('./views/StatefulProxyViewFactory.js');

require('./Apis.js');
require('./Config.js');
require('./EventsFactory.js');
require('./ErrorCodes.js');

require('./services/ServiceBuilderFactory.js');
require('./services/UserService.js');
require('./services/CatchError.js');
require('./services/AuthServiceFactory.js');
require('./services/LoadContextService.js');
require('./services/RecordService.js');

require('./components/Navigation.js');

require('./helpers/Helpers.js');

require('./JavaModule.js');
