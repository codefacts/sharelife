import composer from './core/Composer.js';

composer.addAndReturnFactory('Apis', [],
() => {
  return {
    loginApi: "/api/login",
    logoutApi: "/api/logout",
    usersApi: '/api/users',
    checkPhoneExistsApi: '/api/users/is-registered',
    recordsApi: '/api/records',
  };
});
