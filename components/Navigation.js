import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('Navigation',
[],
() => {
  var navigation = null;

  const setNavigation = (nav) => {
    u.requireNonNull(nav);
    navigation = nav;
  };
  const navigate = (address, msg) => {
    navigation.navigate(address, msg);
  };
  const goBack = () => navigation.goBack();

  const params = () => {
    return navigation.state.params;
  };

  return {
    setNavigation, navigate, goBack, params
  };
});
