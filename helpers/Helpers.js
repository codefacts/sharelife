import composer from '../core/Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('Helpers',
[],
() => {

  const splitNames = (name) => {

    if (!name) {
      return {firstName: ''};
    }

    const split = name.split(' ');

    if (split.length <= 1) {
      return {firstName: name.trim()};
    }

    const firstName = split.slice(0, split.length - 1).join(' ');
    const lastName = split[split.length - 1];

    return {firstName, lastName};
  };

  return {splitNames};
});
