import composer from './Composer.js';
import { AppRegistry, View, Text, TextInput, Button, Alert, AsyncStorage } from 'react-native';

export default composer.addAndReturnFactory('LocalStorage', [], function () {
  var $this = {

    set: function (key, jsonValue) {
      var serialized = JSON.stringify(jsonValue);
      return AsyncStorage.setItem(key, serialized);
    },

    get: function (key) {
      return AsyncStorage.getItem(key)
        .then(str => {
          if (!str) {
            return null;
          }

          return JSON.parse(str);
        });
    },

    clear: function (key) {
      return AsyncStorage.removeItem(key);
    }
  };

  return $this;
});
