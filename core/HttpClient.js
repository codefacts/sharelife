var axios = require('axios');
import composer from './Composer.js';
import u from '../utils/utils.js';

export default composer.addAndReturnFactory('HttpClient',
['Config', 'LocalStorage', 'EventBus', 'Events'],
function ({Config, LocalStorage, EventBus, Events}) {

  var instance = axios.create({
    baseURL: Config.apiBaseUrl,
    timeout: 360000,
    headers: {}
  });

  instance.interceptors.request.use(function (config) {

    return LocalStorage.get('authData')
      .then(authData => {
        var headers = {};

        if (!!authData) {
          headers['Authorization'] = 'Bearer ' + authData.authToken;
        }

        config.headers = u.merge(config.headers, headers);

        return config;
      });

  }, function (error) {
    console.error('request.error', error)
    return Promise.reject(error);
  });

  instance.interceptors.response.use(function (response) {
    return response;
  }, function (error) {
    console.error('http error: ', error);
    if (!!error.response && error.response.status === 401) {
        console.error('### unauthorizedApiAccessError ###');
        EventBus.emit(Events.unauthorizedApiAccessError);
    }
    return Promise.reject(error);
  });

  return instance;

});
