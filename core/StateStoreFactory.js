import composer from './Composer.js';
import u from '../utils/utils.js';

composer.addAndReturnFactory('StateStore',
[],
() => {
  return (initialState) => {

    var state = initialState || {};

    const set = (newState) => {
      state = assign(state, newState);
    };

    const get = () => {
      return state;
    };

    return {
      set, get
    };
  };
});

const assign = (prevState, newState) => {
  const m_state = {};

  copy(prevState, m_state);

  for(var key in newState) {
    const val = newState[key];
    if (val === null || val === undefined) {
      delete m_state[key];
    } else {
      m_state[key] = newState[key];
    }
  }

  return m_state;
};

const copy = (src, dst) => {
  for (var key in src) {
    dst[key] = src[key];
  }
  return dst;
};
