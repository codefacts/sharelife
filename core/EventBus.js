import composer from './Composer.js';
var EventBus = require('eventemitter3');

export default composer.addAndReturnFactory('EventBus',
[],
() => {
  return new EventBus();
});
