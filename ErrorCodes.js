import composer from './core/Composer.js';

composer.addAndReturnFactory('ErrorCodes', [],
() => {
  return {
    unauthorized: 'unauthorized'
  };
});
