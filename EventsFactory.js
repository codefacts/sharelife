import composer from './core/Composer.js';

export default composer.addAndReturnFactory('Events', [], function () {
  return {
    appInitialized: 'appInitialized',
    updateUi: 'updateUi',
    mapInitialized: 'mapInitialized',
    authenticationSuccess: 'authenticationSuccess',
    unauthorizedApiAccessError: 'unauthorizedApiAccessError',
    logout: 'logout',
    login: 'login',
    componentDidMount: 'componentDidMount'
  };
});
