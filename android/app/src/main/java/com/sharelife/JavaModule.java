package com.sharelife;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Promise;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

public class JavaModule extends ReactContextBaseJavaModule {

    private static final String E_PICKER_CANCELLED = "E_PICKER_CANCELLED";
    private static final String E_NO_IMAGE_DATA_FOUND = "E_NO_IMAGE_DATA_FOUND";
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_FAILED_TO_SHOW_PICKER = "E_FAILED_TO_SHOW_PICKER";
    private Promise takePicturePromise;
    private Promise openGalleryPromise;
    private String takePictureUri;

    public JavaModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "JavaModule";
    }

    private static final String TAG = JavaModule.class.getSimpleName();
    public static final int CAPTURE_IMAGE_REQUEST_CODE = 1;
    public static final int PICK_IMAGE_REQUEST_CODE = 2;
    public String IMAGE_DIR = "/images";

    @ReactMethod
    void openGallery(Promise promise) {
        try {
            Activity currentActivity = getCurrentActivity();

            if (currentActivity == null) {
                promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
                return;
            }

            // Store the promise to resolve/reject when picker returns data
            openGalleryPromise = promise;
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            Log.i(TAG, "openGallery: Starting Activity File Chooser");
            currentActivity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE);
        } catch (Exception ex) {
            openGalleryPromise.reject(E_FAILED_TO_SHOW_PICKER, ex);
            openGalleryPromise = null;
        }
    }

    @ReactMethod
    void takePicture(String imageFileName, Promise promise) {

        try {

            Activity currentActivity = getCurrentActivity();

            if (currentActivity == null) {
                promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
                return;
            }

            Log.i(TAG, "captureNewImage: Creating New File for Capture New Image");

            String imageDir = createImageDir(currentActivity, IMAGE_DIR);

            File imageFile = new File(imageDir, imageFileName);

            try {
                if (!imageFile.exists()) {
                    imageFile.createNewFile();
                }
            } catch (IOException e) {
                throw new UnexpectedException(e);
            }

//                Uri uri = Uri.fromFile(imageFile);

            final Uri uri = Build.VERSION.SDK_INT < 24 ? Uri.fromFile(imageFile) : FileProvider.getUriForFile(currentActivity, "com.sharelife.file.provider", imageFile);

            takePictureUri = uri.toString();
            Log.i(TAG, "captureNewImage: File Uri: " + uri.toString());

            takePicturePromise = promise;

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

            if (intent.resolveActivity(currentActivity.getPackageManager()) == null) {
                throw new UnexpectedException("No App exists in the system for image capture");
            }

            currentActivity.startActivityForResult(intent, CAPTURE_IMAGE_REQUEST_CODE);

        } catch (Exception ex) {
            takePicturePromise.reject(E_FAILED_TO_SHOW_PICKER, ex);
            takePicturePromise = null;
        }
    }

    //ImageDir must startsWith '/'
    public static String createImageDir(Context context, String imagesDir) {

        String rootPath = getStorageDir(context).getAbsolutePath();

        File file = new File(rootPath + imagesDir);

        Log.i(TAG, "createImageDir: imageDir: " + file.getAbsolutePath());

        if (file.exists()) {
            return file.getAbsolutePath();
        }

        try {

            boolean mkdirs = file.mkdirs();

            if (!mkdirs) {
                throw new UnexpectedException("Image dir could not be created: '" + file + "'");
            }

        } catch (Exception e) {
            Log.e(TAG, "createImageDir: error in creating images dir", e);
            throw new UnexpectedException(e);
        }

        Log.i(TAG, "createImageDir: Image Dir created: " + file.getAbsolutePath());

        return file.getAbsolutePath();
    }

    public static File getStorageDir(Context context) {
        if (isExternalStorageWritable() && isExternalStorageReadable()) {
            return context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        }
        return context.getFilesDir();
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            Log.i(TAG, "onActivityResult: requestCode = " + requestCode + " | resultCode = " + resultCode);
            if (requestCode == CAPTURE_IMAGE_REQUEST_CODE) {
                if (takePicturePromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        takePicturePromise.reject(E_PICKER_CANCELLED, "Image picker was cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        Log.i(TAG, "onActivityResult: takePicturePromise => " + takePictureUri);
                        takePicturePromise.resolve(takePictureUri);
                    }

                    takePicturePromise = null;
                }
            } else if (requestCode == PICK_IMAGE_REQUEST_CODE) {
                Log.i(TAG, "onActivityResult: PICK_IMAGE_REQUEST_CODE");
                if (openGalleryPromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        openGalleryPromise.reject(E_PICKER_CANCELLED, "Image picker was cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        Uri uri = intent.getData();

                        if (uri == null) {
                            openGalleryPromise.reject(E_NO_IMAGE_DATA_FOUND, "No image data found");
                        } else {
                            Log.i(TAG, "onActivityResult: openGalleryPromise => " + uri);
                            openGalleryPromise.resolve(uri.toString());
                        }
                    }

                    openGalleryPromise = null;
                }
            }
        }
    };
}