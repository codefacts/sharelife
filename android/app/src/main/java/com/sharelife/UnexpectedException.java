package com.sharelife;

/**
 * Created by skpaul on 18/03/06.
 */

final class UnexpectedException extends RuntimeException {
    public UnexpectedException(Throwable cause) {
        super(cause);
    }

    public UnexpectedException(String msg) {
        super(msg);
    }
}
